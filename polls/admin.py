from django.contrib import admin

from .models import Choice, Question

# Update UI for admin side, we have to add some methods on this file.

# Show choices in quetions section and Add Extra choices with tabular form
class ChoiceInline(admin.TabularInline):
    model = Choice
    # extra = 3

# Quetion section update UI or add some more functionality, we can use this methods.
class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['question_text']}),
        ('Date information1', {'fields': [
         'pub_date'], 'classes': ['collapse']}),
    ]
    inlines = [ChoiceInline]
    list_display = ('question_text', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']
    search_fields = ['question_text']



admin.site.register(Question, QuestionAdmin)
